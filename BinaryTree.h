#pragma once

struct BinaryTree{
    struct BinaryTree *left;
    struct BinaryTree *right;
    int data = 0;
    bool hasData = false;

    bool Insert(int d){
        if(!hasData){
            hasData = true;
            data = d;
        }else{
           left = new BinaryTree(); 
           right = new BinaryTree();
           if(d < data){
               return left.Insert(d);
           }
           if(d > data){
               return right.Insert(d);
           }
        }

    }

};
