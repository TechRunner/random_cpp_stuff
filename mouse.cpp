#include <iostream>
#include <fstream>


int main(){
    std::ifstream mouse("/dev/input/by-path/platform-INT33C3:00-mouse");
    if(!mouse.is_open()){
        std::cout << "Mouse failed to open\n";
        return -1;
    }
    char x, y;

    while(mouse >> x >> y){
        std::cout << "x: " << (int) x << " y: " << (int) y << "\n";
    }
}
