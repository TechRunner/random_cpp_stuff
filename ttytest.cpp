#include <fstream>
#include <string>

int main(){
    std::ofstream tty("/dev/tty");
    std::ifstream ttyin("/dev/tty");

    if(!tty.is_open()){
        tty.close();
    }

    std::string input;
    getline(ttyin, input);
    tty << input << "\n";
}

