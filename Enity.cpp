class Entity{
public:    
    int m_DefHealth = 10;
public:
    std::string s_name;
    float x, y, f_speed;
    int n_health;
    int n_maxHealth;
    bool b_visable;
    bool b_alive = true;
    
    Entity(std::string name){
        n_health = m_DefHealth;
        s_name = name;
        x = 0;
        y = 0;
        b_visable = false;
    }
    
    void Move(float x1, float y1){
        x += x1 * f_speed;
        y += y1 * f_speed;
    }
    
    void TakeDamage(int amount){
        if(n_health - amount < 0){
            b_alive = false;
        }else{
            n_health -= amount;
        }
    }
    
    void Heal(int amount){
        n_health = (n_health + amount < n_maxHealth) ? 
            n_health + amount : n_maxHealth;
       void TakeDamage(int amount){
        if(n_health - amount < 0){
            b_alive = false;
        }else{
            n_health -= amount;
        }
    }
    
    void Heal(int amount){
        n_health = (n_health + amount < n_maxHealth) ? 
            n_health + amount : n_maxHealth;
    }}
};
