#include <iostream>

int *stack = new int();
int *sptr = stack;

void push(int var){
    *sptr = var;
    sptr += 1;
}

int pop(){
    if(sptr > stack){
        sptr -= 1;
    }
    int tmp = *sptr;
    return tmp;
}

int main(){
    push(4);
    push(5);
    push(3);
    std::cout << pop() << "\n";
    std::cout << pop() << "\n";
    std::cout << pop() << "\n";
}
