#pragma once

struct node{
    char* name;
    char* data;
};

class Config{
private:
    char* m_filepath;
    node *nodes;
    int count;
    char* m_filedata;

private:
    int getVarCount();
    char* getFileData();
    
public:
    Config(char* filepath);
    char* getString(char* varname);
    int getInt(char* varname);

};
